# Copyright 2010 Timothy Redaelli <timothy@redaelli.eu>
# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam lang_files[]
exparam -v LANG_FILES lang_files[@]

myexparam lang_pv=${PV}
exparam -v LANG_PV lang_pv

myexparam docs_pv=${PV}
exparam -v DOCS_PV docs_pv

require github [ user="tesseract-ocr" tag=${PV} ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

export_exlib_phases src_unpack src_prepare src_compile src_install

SUMMARY="A commercial quality OCR engine developed at HP in the 80's and early 90's"
DESCRIPTION="
The Tesseract OCR engine was one of the top 3 engines in the 1995 UNLV Accuracy
test. Between 1995 and 2006 it had little work done on it, but it is probably one
of the most accurate open source OCR engines available.
The source code will read a binary, grey or color image and output text.
"
HOMEPAGE+=" https://github.com/${PN}-ocr"

lang_traineddata_link() {
    case "$LANG_PV" in
    scm-*)
        echo -n " https://github.com/tesseract-ocr/tessdata/raw/${LANG_PV#scm-}/${1}.traineddata -> ${PN}-data-${1}-${LANG_PV}.traineddata"
        ;;
    *)
        echo -n " https://dev.exherbo.org/~philantrop/distfiles/${PN}-data-${1}-${LANG_PV}.tar.xz"
        ;;
    esac
}

set_languages_downloads() {
    local base_url="https://dev.exherbo.org/~philantrop/distfiles" x
    for x in "${LANG_FILES[@]}"; do
        echo -n "linguas:${x% *}? ( $(lang_traineddata_link ${x#* }) ) "
    done
    # Auto orientation and script detection requested, but osd language failed to load
    lang_traineddata_link osd
}

set_languages_options() {
    local lang
    for lang in "${LANG_FILES[@]}"; do
        if [[ ${lang% *} == aze ]]; then
            echo -n "${lang% *} [[ description = [ Azerbaijani locale ] ]] "
        elif [[ ${lang% *} == chr ]]; then
            echo -n "${lang% *} [[ description = [ Cherokee locale ] ]] "
        elif [[ ${lang% *} == enm ]]; then
            echo -n "${lang% *} [[ description = [ Middle English (1100-1500) locale ] ]] "
        elif [[ ${lang% *} == epo_alt ]]; then
            echo -n "${lang% *} [[ description = [ Alternative Esperanto locale ] ]] "
        elif [[ ${lang% *} == equ ]]; then
            echo -n "${lang% *} [[ description = [ Math / equation detection module ] ]] "
        elif [[ ${lang% *} == frk ]]; then
            echo -n "${lang% *} [[ description = [ Frankish locale ] ]] "
        elif [[ ${lang% *} == frm ]]; then
            echo -n "${lang% *} [[ description = [ Middle French (ca. 1400-1600) locale ] ]] "
        elif [[ ${lang% *} == grc ]]; then
            echo -n "${lang% *} [[ description = [ Ancient Greek locale ] ]] "
        elif [[ ${lang% *} == ita_old ]]; then
            echo -n "${lang% *} [[ description = [ Italian (Old) locale ] ]] "
        elif [[ ${lang% *} == spa_old ]]; then
            echo -n "${lang% *} [[ description = [ Spanish (Old) locale ] ]] "
        elif [[ ${lang% *} == zh_simplified ]]; then
            echo -n "${lang% *} [[ description = [ Simplified Chinese locale ] ]] "
        elif [[ ${lang% *} == zh_traditional ]]; then
            echo -n "${lang% *} [[ description = [ Traditional Chinese locale ] ]] "
        else
            echo -n "${lang% *} "
        fi
    done
}

DOWNLOADS+="
    $(set_languages_downloads)

    https://dev.exherbo.org/~philantrop/distfiles/${PN}-docs-${DOCS_PV}.tar.xz
"

BUGS_TO="philantrop@exherbo.org"
LICENCES="Apache-2.0"
SLOT="0"

MYOPTIONS="
    debug
    ( linguas: ( $(set_languages_options) ) ) [[ number-selected = at-least-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/glib:2
        dev-libs/icu:=
        media-libs/fontconfig
        media-libs/leptonica[>=1.74]
        media-libs/libpng:=
        media-libs/tiff
        x11-libs/cairo
        x11-libs/pango[>=1.38]
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-graphics
    --disable-embedded
    --disable-opencl
    --disable-static
    # Visibility triggers static linking and that triggers a build failure
    --disable-visibility
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
)

DEFAULT_SRC_INSTALL_PARAMS=(
    install
    install-langs
    training-install
)

WORK=${WORKBASE}/${PNV/-rc1}

tesseract_src_unpack() {
    default

    local f t
    for f in ${ARCHIVES}; do
        case "$f" in
        *.traineddata)
            # get real name by stripping package name prefix and version suffix
            t=${f#${PN}-data-}
            t=${t/-${LANG_PV}}
            edo cp "${FETCHEDDIR}/$f" "${WORKBASE}/$t"
            ;;
        esac
    done
}

tesseract_src_prepare() {
    # Copy all the data into the appropriate directories
    edo find "${WORKBASE}" -maxdepth 1 -type f -exec cp -v {} "${WORK}"/tessdata/ \;

    autotools_src_prepare
}

tesseract_src_compile() {
    default

    # The training tools need to be built and installed after the main part
    emake training
}

tesseract_src_install() {
    default

    # install-langs doesn't work for god-knows-what reason
    edo find "${WORKBASE}" -maxdepth 1 -type f -exec cp -v {} "${IMAGE}"/usr/share/tessdata/ \;

    dodoc "${WORKBASE}"/${PN}-docs-${DOCS_PV}/*
}

