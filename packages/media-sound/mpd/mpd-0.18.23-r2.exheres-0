# Copyright 2008 Richard Brown
# Copyright 2009 Bo Ørsted Andresen
# Copyright 2009 Thomas Anderson
# Copyright 2011 Ali Polatel
# Copyright 2011-2015 Quentin "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="A flexible, powerful, server-side application for playing music"
DESCRIPTION="
Music Player Daemon (MPD) is a flexible, powerful, server-side application for playing music.
Through plugins and libraries it can play a variety of sound files while being controlled by
its network protocol.
"
HOMEPAGE="http://www.musicpd.org/"
DOWNLOADS="http://www.musicpd.org/download/${PN}/$(ever range -2)/${PNV}.tar.xz"
UPSTREAM_RELEASE_NOTES="http://git.musicpd.org/cgit/master/${PN}.git/plain/NEWS?h=v${PV}"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="aac alsa avahi doc libsamplerate ogg openal opus pulseaudio sndfile tcpd
    cdio         [[ description = [ CD support through libcdio ] ]]
    curl         [[ description = [ Support obtaining song data via HTTP ] ]]
    dsd          [[ description = [ Support for decoding DSD ] ]]
    id3          [[ description = [ Support for ID3 tags ] ]]
    jack         [[ description = [ Enable jack-audio-connection-kit audio output ] ]]
    libmpdclient [[ description = [ Enable support for remote mpd databases ] ]]
    mms          [[ description = [ Microsoft Media Server protocol support ] ]]
    opus         [[ description = [ Opus codec support ] requires = ogg ]]
    shout        [[ description = [ Enable support for streaming through shout (mp3, and ogg if ogg is enabled) ] ]]
    soundcloud   [[ description = [ SoundCloud.com support (input) ] ]]
    sqlite       [[ description = [ Enable support for storing the MPD database in an Sqlite database ] ]]
    systemd      [[ description = [ systemd socket activation support ] ]]
    zip          [[ description = [ zip archive support ] ]]
    (
        aac mp3 mikmod musepack wavpack
        audiofile [[ description = [ Enable audiofile support, enables wave support ] ]]
        ffmpeg    [[ description = [ Enable the ffmpeg input plugin, allowing you to play all audio formats supported by ffmpeg/libav ] ]]
        modplug   [[ description = [ mod-like file format support ] ]]
        ( flac shout vorbis ) [[ requires = ogg ]]
    ) [[ number-selected = at-least-one ]]
    ffmpeg? ( ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            app-text/docbook-xml-dtd:4.2
            app-text/xmlto
            app-doc/doxygen
        )
    build+run:
        app-arch/bzip2
        dev-libs/glib:2[>=2.28]
        aac? ( media-libs/faad2 )
        alsa? ( sys-sound/alsa-lib[>=0.9.0] )
        audiofile? ( media-libs/audiofile[>=0.3] )
        avahi? ( net-dns/avahi )
        cdio? (
            dev-libs/libcdio
            dev-libs/libcdio-paranoia
        )
        curl? ( net-misc/curl[>=7.18] )
        flac? ( media-libs/flac[>=1.2][ogg?] )
        id3? ( media-libs/libid3tag )
        jack? ( media-sound/jack-audio-connection-kit )
        libmpdclient? ( media-libs/libmpdclient[>=2.2] )
        libsamplerate? ( media-libs/libsamplerate[>=0.0.15] )
        mms? ( media-libs/libmms[>=0.4] )
        mikmod? ( media-libs/libmikmod[>=3.3.6] )
        modplug? ( media-libs/libmodplug )
        mp3? (
            media-libs/libmad
            media-sound/lame
        )
        musepack? ( media-libs/musepack )
        ogg? ( media-libs/libogg )
        openal? ( media-libs/openal )
        opus? ( media-libs/opus )
        ffmpeg? (
            providers:ffmpeg? ( media/ffmpeg )
            providers:libav? ( media/libav )
        )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.16] )
        shout? ( media-libs/libshout )
        sndfile? ( media-libs/libsndfile )
        soundcloud? ( dev-libs/yajl[>=2.0] )
        sqlite? ( dev-db/sqlite:3 )
        systemd? ( sys-apps/systemd )
        tcpd? ( sys-apps/tcp-wrappers )
        vorbis? ( media-libs/libvorbis )
        wavpack? ( media-sound/wavpack )
        zip? ( dev-libs/zziplib[>=0.13] )
    test:
        dev-cpp/cppunit
    suggestion:
        media-sound/ario       [[ description = [ Provides rhythmbox-like client ] ]]
        media-sound/gmpc       [[ description = [ Provides fast and fully featured GTK-based client ] ]]
        media-sound/mpc        [[ description = [ Provides command line client ] ]]
        media-sound/mpdcron    [[ description = [ Executes scripts based on mpd's idle events ] ]]
        media-sound/ncmpc      [[ description = [ Provides ncurses based command line client ] ]]
        media-sound/pms        [[ description = [ Provides an alternative ncurses based command line client ] ]]
        media-sound/qmpdclient [[ description = [ Provides simple QT client ] ]]
        media-sound/sonata     [[ description = [ Provides an elegant GTK-based client ] ]]
        sys-sound/oss          [[ description = [ Provides an alternative sound architecture instead of ALSA ] ]]
"

BUGS_TO="sardemff7@exherbo.org"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-configure.ac-Search-for-libsystemd-instead-of-libsys.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-bzip2
    --enable-fifo
    --enable-httpd-output
    --enable-inotify
    --enable-ipv6
    --enable-oss
    --enable-pipe-output
    --enable-recorder-output
    --enable-tcp
    --enable-un
    --enable-wave-encoder
    --disable-adplug
    --disable-ao
    --disable-fluidsynth
    --disable-gme
    --disable-iso9660
    --disable-mpg123
    --disable-roar
    --disable-sidplay
    --disable-twolame-encoder
    --disable-werror
    --disable-wildmidi
    --without-tremor
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    aac
    alsa
    audiofile
    'cdio cdio-paranoia'
    curl
    'doc documentation'
    dsd
    ffmpeg
    flac
    id3
    jack
    libmpdclient
    'libsamplerate lsr'
    mms
    mikmod
    modplug
    'mp3 mad'
    'mp3 lame-encoder'
    'musepack mpc'
    vorbis
    'vorbis vorbis-encoder'
    openal
    opus
    'pulseaudio pulse'
    shout
    sndfile
    soundcloud
    sqlite
    'systemd systemd-daemon'
    'tcpd libwrap'
    wavpack
    'zip zzip'
)

DEFAULT_SRC_CONFIGURE_TESTS=( '--enable-test --disable-test' )

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'avahi zeroconf avahi'
    'systemd systemdsystemunitdir'
)

