# Copyright 2012,2013 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Command-line utilities to encode, inspect, and decode .opus files"
HOMEPAGE="https://opus-codec.org"
DOWNLOADS="https://archive.mozilla.org/pub/opus/${PNV}.tar.gz"

BUGS_TO="mixi@exherbo.org"

LICENCES="
    BSD-2
    GPL-2 [[ note = [ opusinfo ] ]]
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    flac [[ description = [ Support for encoding from flac files ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libpcap
        media-libs/libogg[>=1.3]
        media-libs/libopusenc[>=0.2]
        media-libs/opus[>=1.0.3]
        media-libs/opusfile[>=0.5]
        flac? ( media-libs/flac[>=1.1.3] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # this option just enables -msse
    --disable-sse
    --disable-static
    # The package incorrectly and uselessly checks for pkg-config before using
    # the PKG_CHECK_* m4 macros. Override this.
    HAVE_PKG_CONFIG=yes
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'flac'
)

