# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="FFmpeg support for GStreamer"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="GPL-2 LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk-doc
    system-ffmpeg [[ description = [ Use system ffmpeg libraries ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.11.5]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.12] )
        !system-ffmpeg? ( dev-lang/yasm )
    build+run:
        dev-libs/glib:2[>=2.40.0]
        dev-libs/orc:0.4[>=0.4.16]
        media-libs/gstreamer:1.0[>=${PV}]
        media-plugins/gst-plugins-base:1.0[>=${PV}]
        system-ffmpeg? ( media/ffmpeg[>=3.2.4] [[ note = [ bundled ffmpeg is 3.2.4 ] ]] )
        !media-plugins/gst-ffmpeg[=1*] [[
            description = [ Wrong package was bumped, please downgrade gst-ffmpeg ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-gpl'
    '--enable-orc'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gtk-doc' )
# FIXME: Option wasn't renamed yet upstream
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'system-ffmpeg system-libav' )

DEFAULT_SRC_COMPILE_PARAMS=( RANLIB="${RANLIB}" ERROR_CFLAGS="" )

