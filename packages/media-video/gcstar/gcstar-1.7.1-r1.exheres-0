# Copyright 2009 Jonathan Dahan <jedahan@gmail.com>
# Copyright 2011 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome-2 freedesktop-desktop freedesktop-mime

SUMMARY="A personal collections manager"
HOMEPAGE="http://www.${PN}.org/"
DOWNLOADS="http://download.gna.org/${PN}/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"

BUGS_TO="alip@exherbo.org"

# Note: installation requires "en" to be present so it is not mentioned as
#       option
LANGS="ar bg ca cs de el es fr gl hu id it nl pl pt ro ru sr sv tr uk zh"
MYOPTIONS="mp3 vorbis linguas: ${LANGS}"

# TODO: cddb?  ( dev-perl/Net-FreeDB )
# TODO: spell? ( dev-perl/Gtk2-Spell )
# TODO: gnome? ( dev-perl/Gnome2-VFS )
DEPENDENCIES="
    build+run:
        dev-lang/perl:=[>=5.18.2]
        dev-perl/Archive-Zip
        dev-perl/Date-Calc
        dev-perl/DateTime-Format-Strptime
        dev-perl/GD
        dev-perl/GDGraph
        dev-perl/Gtk2-Perl
        dev-perl/HTML-Parser
        dev-perl/IO-Compress
        dev-perl/libwww-perl
        dev-perl/URI
        dev-perl/XML-Parser
        dev-perl/XML-Simple
        mp3? (
            dev-perl/MP3-Info
            dev-perl/MP3-Tag
        )
        vorbis? ( dev-perl/Ogg-Vorbis-Header-PurePerl )
"

WORK=${WORKBASE}/${PN}

src_prepare() {
    default

    # Remove unused languages
    for lang in ${LANGS}; do
        if ! option linguas:${lang}; then
            edo rm -rf "${WORK}"/lib/${PN}/GCLang/$(echo ${lang} | tr '[:lower:]' '[:upper:]')
        fi
    done
}

src_compile() {
    :
}

src_install() {
    edo ./install --noclean --nomenu --prefix="${IMAGE}"/usr

    # TODO: patch install to honor prefix in installDesktop
    insinto /usr/share/pixmaps
    edo mv share/gcstar/icons/{${PN}_64x64,${PN}}.png
    doins share/gcstar/icons/${PN}.png
    insinto /usr/share/applications
    doins share/applications/${PN}.desktop
    insinto /usr/share/mime/packages
    doins share/applications/${PN}.xml

    dodir /usr/$(exhost --target)
    dodir /usr/share
    edo mv "${IMAGE}"/usr/{bin,lib} "${IMAGE}"/usr/$(exhost --target)/
    edo mv "${IMAGE}"/usr/man "${IMAGE}"/usr/share/

    edo sed -i -e "/GCS_SHARE_DIR/s:= .*:= '/usr/share/gcstar/');:" "${IMAGE}"/usr/$(exhost --target)/bin/gcstar
}

pkg_postinst() {
    freedesktop-desktop_update_desktop_database
    freedesktop-mime_update_mime_database
}

