# Copyright 2008 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2012-2014 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# last checked revision: 8f8bc92365a943e96cc08b5369408c20f35193c7

require toolchain-funcs option-renames [ renames=[ 'vp8 vpx' ] ]

if ever is_scm; then
    SCM_REPOSITORY="git://git.libav.org/libav.git"
    require scm-git
else
    DOWNLOADS="http://libav.org/releases/${PNV}.tar.xz"
fi

export_exlib_phases pkg_setup src_configure src_test src_compile src_install

SUMMARY="A complete solution to record, convert and stream audio and video"

HOMEPAGE="http://libav.org/"

GPL_OPTIONS=( cd h264 xavs hevc )
V3_OPTIONS=( aac opencore )
NONFREE_OPTIONS=( fdk-aac )

NONFREE_PROVIDERS=( libressl openssl )

if ! ever at_least 12; then
    GPL_OPTIONS+=( X )
fi

LICENCES="LGPL-2.1"

for opt in "${GPL_OPTIONS[@]}"; do
    LICENCES+=" ${opt}? ( GPL-2 )"
done
for opt in "${V3_OPTIONS[@]}"; do
    LICENCES+=" ${opt}? ( LGPL-3 )"
done

BUGS_TO="mixi@exherbo.org"

UPSTREAM_CHANGELOG="http://libav.org/releases/${PNV}.changelog"

SLOT="0"

# TODO: the yasm dep requires platform:, but that's not reliable when cross-compiling
MYOPTIONS="
    X            [[ description = [ Enable support for X11 grabbing ] ]]
    aac          [[ description = [ Additional AAC encoder plugin (libav's encoder is experimental) ] ]]
    cd           [[ description = [ Audio CD grabbing ] ]]
    dirac        [[ description = [ Support encoding and decoding dirac ] ]]
    fdk-aac      [[ description = [ Support for AAC encoding using the Frauenhofer AAC Codec Library, makes the resulting binary non-redistributable ] ]]
    frei0r       [[ description = [ Video effects using frei0r-plugins ] ]]
    gsm          [[ description = [ Support for GSM codec (audio), mainly for telephony ] ]]
    h264
    hevc         [[ description = [ Enable H.265/HEVC encoding using x265 ] ]]
    ieee1394     [[ description = [ Enable IIDC-1394 grabbing using libdc1394 ] ]]
    ilbc         [[ description = [ Support for en/decoding iLBC (audio codec) via libilbc ] ]]
    jpeg2000     [[ description = [ Support for the lossy image compression format ] ]]
    mp3          [[ description = [ Support for mp3 encoding with lame ] ]]
    opencore     [[ description = [ Support for OpenCORE AMR-WB decoder and encoder (audio) ] ]]
    opencv       [[ description = [ Computer Vision techniques from OpenCV ] note = [ untested ] ]]
    opus         [[ description = [ Support for en/decoding opus (audio codec) via libopus ] ]]
    player       [[ description = [ Libav reference audio and video player software ] ]]
    pulseaudio   [[ description = [ Pulseaudio capture support ] ]]
    speex        [[ description = [ Enable support for decoding and encoding audio using libspeex ] ]]
    theora       [[ description = [ Enable support for encoding using the Theora Video Compression Codec ] ]]
    va           [[ description = [ Enable support for decoding video using the Video Acceleration API ] ]]
    vdpau        [[ description = [ Enable support for VDPAU hardware accelerated video decoding ] ]]
    vorbis       [[ description = [ Additional OggVorbis audio encoder plugin (ffmpeg's encoder is experimental) ] ]]
    vpx          [[ description = [ Enable support for the VP8 video compression format ] ]]
    xavs         [[ description = [ Support AVS, the Audio Video Standard of China ] ]]

    doc

    (
        providers:
            gnutls
            libressl [[ description = [ Use LibreSSL as the SSL provider, makes the resulting binary non-redistributable ] ]]
            openssl [[ description = [ Use OpenSSL as the SSL provider, makes the resulting binary non-redistributable ] ]]
    ) [[ number-selected = exactly-one ]]

    ( platform: x86 amd64 )
"

MYOPTIONS+="bindist [[ requires = ["
for opt in "${NONFREE_OPTIONS[@]}"; do
    MYOPTIONS+=" -${opt}"
done
for opt in "${NONFREE_PROVIDERS[@]}"; do
    MYOPTIONS+=" providers: -${opt}"
done
MYOPTIONS+=" ] ]]"

DEPENDENCIES="
    build:
        app-arch/xz
        dev-lang/perl:*
        doc? ( app-text/texi2html app-doc/doxygen )
        platform:amd64? ( dev-lang/yasm )
        platform:x86? ( dev-lang/yasm )
    build+run:
        app-arch/bzip2
        media-libs/fontconfig
        media-libs/freetype:2
        sys-libs/zlib
        sys-sound/alsa-lib
        aac? ( media-libs/vo-aacenc )
        cd? ( dev-libs/libcdio-paranoia )
        dirac? ( media-libs/schroedinger )
        fdk-aac? ( media-libs/fdk-aac )
        frei0r? ( media-plugins/frei0r-plugins )
        gsm? ( media-libs/gsm )
        h264? ( media-libs/x264[>=20120104] )
        ieee1394? (
            media-libs/libdc1394:2
            media-libs/libraw1394
        )
        ilbc?   ( media-libs/ilbc )
        jpeg2000? ( media-libs/OpenJPEG:0 )
        mp3? ( media-sound/lame[>=3.98.3] )
        opencore? (
            media-libs/opencore-amr
            media-libs/vo-amrwbenc
        )
        opencv? ( media-libs/opencv )
        opus?   ( media-libs/opus )
        player? ( media-libs/SDL:0[>=1.2.1] )
        providers:gnutls? (
            dev-libs/gmp:=
            dev-libs/gnutls
            dev-libs/libgcrypt
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        pulseaudio? ( media-sound/pulseaudio )
        speex? ( media-libs/speex )
        theora? (
            media-libs/libtheora
            media-libs/libogg
        )
        va? ( x11-libs/libva[>=1.0.6] )
        vdpau? ( x11-libs/libvdpau[>=0.2] )
        vorbis? (
            media-libs/libogg
            media-libs/libvorbis
        )
        vpx? ( media-libs/libvpx[>=0.9.6] )
        xavs? ( media-libs/xavs )

        !media/ffmpeg [[ description = [ This is a fork of ffmpeg and uses the same file names ] ]]
"

if ever at_least 12; then
    DEPENDENCIES+="
        build+run:
            x11-libs/libX11 [[ note = [ automagic ] ]]
            X? ( x11-libs/libxcb )
            hevc? ( media-libs/x265[>=1.7] ) [[ note = [ commit 10362:5d1b7ed7ad18 ] ]]
"
else
    DEPENDENCIES+="
        build+run:
            X? (
                x11-libs/libX11
                x11-libs/libXext
                x11-libs/libXfixes
            )
            hevc? ( media-libs/x265 )
            providers:gnutls? ( dev-libs/nettle )
"
fi

LIBAV_LIBDIRS=(
    libavcodec
    libavdevice
    libavfilter
    libavformat
    libavresample
    libavutil
    libswscale
)

libav-cc_define_enable() {
    cc-has-defined ${1} && echo --enable-${2} || echo --disable-${2}
}

libav_pkg_setup() {
    export V=1
}

libav_src_test() {
    LD_LIBRARY_PATH=$(IFS=:; echo "${LIBAV_LIBDIRS[*]}") \
        emake fate
}

# grayscale support is disabled
libav_src_configure() {
    local myconf=() target=$(exhost --target)

    # licences
    local opt gpl_param="" v3_param="" nonfree_param=""

    for opt in "${GPL_OPTIONS[@]}"; do
        option "${opt}" && gpl_param="--enable-gpl"
    done
    for opt in "${V3_OPTIONS[@]}"; do
        option "${opt}" && v3_param="--enable-version3" # LGPL-3 or GPL-3 depending on gpl_param
    done
    for opt in "${NONFREE_OPTIONS[@]}"; do
        option "${opt}" && nonfree_param="--enable-nonfree" # not redistributable
    done
    for opt in "${NONFREE_PROVIDERS[@]}"; do
        option "providers:${opt}" && nonfree_param="--enable-nonfree" # not redistributable
    done

    myconf=(
        ${gpl_param}
        ${v3_param}
        ${nonfree_param}

        --cross-prefix=$(exhost --target)-
        --datadir=/usr/share/
        --docdir=/usr/share/doc/${PNVR}
        --enable-cross-compile
        --host-cc=$(exhost --build)-gcc
        --mandir=/usr/share/man
        --optflags="${CFLAGS}" # respect user cflags and avoid -O3 -g
        --prefix=/usr/$(exhost --target)
        --target-os=linux

        ### CPU features
        --arch=${target%%-*}

        # x86
        # yasm requires mmx, as configure doesn't properly set YASMOPTS if mmx is disabled
        # this is quite nice as we won't enable yasm on non-x86 as side-effect
        $(libav-cc_define_enable __MMX__ yasm)
        $(libav-cc_define_enable __MMX__ mmx)
        # most of ffmpeg's mmxext functions have an sse2 alternative with higher priority anyways
        # $(libav-cc_define_enable TODO mmxext)
        $(libav-cc_define_enable __3dNOW__ amd3dnow)
        # ffmpeg uses pfpnacc which is part of 3dNOW_A according to gcc sources
        $(libav-cc_define_enable __3dNOW_A__ amd3dnowext)
        $(libav-cc_define_enable __SSE__ sse)
        $(libav-cc_define_enable __SSE2__ sse2)
        $(libav-cc_define_enable __SSE3__ sse3)
        $(libav-cc_define_enable __SSSE3__ ssse3)
        $(libav-cc_define_enable __SSE4_1__ sse4)
        $(libav-cc_define_enable __SSE4_2__ sse42)
        $(libav-cc_define_enable __AVX__ avx)
        $(libav-cc_define_enable __FMA4_ fma4)
        $(libav-cc_define_enable __AVX2__ avx2)
        $(libav-cc_define_enable __FMA__ fma3)
        $(libav-cc_define_enable __XOP__ xop)

        # arm
        $(libav-cc_define_enable __VFP_FP__ vfp)
        $(libav-cc_define_enable __ARM_NEON__ neon)
        # $(libav-cc_define_enable TODO armv5te)
        # $(libav-cc_define_enable TODO armv6)
        # $(libav-cc_define_enable TODO armv6t2)

        # hard enable
        --enable-avconv
        --enable-bzlib
        --enable-libfontconfig
        --enable-libfreetype
        --enable-pthreads
        --enable-zlib
        --enable-avresample

        # system related
        --enable-shared
        --disable-debug
        --disable-optimizations # optimizations is just a stupid way of enabling -O3 -fomit-frame-pointer

        # hooverish upstream
        --disable-libfaac

        ### OPTIONS
        $(option_enable aac libvo-aacenc)
        $(option_enable cd libcdio)
        $(option_enable dirac libschroedinger)
        $(option_enable fdk-aac libfdk-aac)
        $(option_enable frei0r)
        $(option_enable gsm libgsm)
        $(option_enable h264 libx264)
        $(option_enable hevc libx265)
        $(option_enable ieee1394 libdc1394)
        $(option_enable ilbc libilbc)
        $(option_enable jpeg2000 libopenjpeg)
        $(option_enable mp3 libmp3lame)
        $(option_enable opencore libopencore-amrnb)
        $(option_enable opencore libopencore-amrwb)
        $(option_enable opencore libvo-amrwbenc)
        $(option_enable opencv libopencv)
        $(option_enable opus libopus)
        $(option_enable player avplay)
        $(option_enable pulseaudio libpulse)
        $(option_enable speex libspeex)
        $(option_enable theora libtheora)
        $(option_enable va vaapi)
        $(option_enable vdpau)
        $(option_enable vorbis libvorbis)
        $(option_enable vpx libvpx)
        $(option_enable xavs libxavs)
    )

    ## Providers for the SSL library
    myconf+=( $(option_enable providers:gnutls gnutls) )
    if option providers:libressl || option providers:openssl; then
        myconf+=( --enable-openssl )
    else
        myconf+=( --disable-openssl )
    fi

    if ever at_least 12; then
        myconf+=(
            # hwdec
            --disable-cuda # only useful with nvenc
            --disable-d3d11va
            --disable-libmfx
            --disable-mmal
            --disable-nvenc # unpackaged
            --disable-omx
            --disable-omx-rpi

            # en-/decoders
            --disable-libhdcd # unpackaged
            --disable-libkvazaar # unpackaged
            --disable-libopenh264 # unpackaged

            # in-/output
            $(option_enable X libxcb)
            $(option_enable X libxcb-shm)
            $(option_enable X libxcb-xfixes)
        )
    else
        myconf+=(
            $(option_enable X x11grab)
        )
    fi

    edo ./configure "${myconf[@]}"
}

libav_src_compile() {
    emake all \
        $(option doc && echo "apidoc")
}

libav_src_install() {
    # nobody has a proper install target for the api doc
    if option doc; then
        insinto /usr/share/doc/${PNVR}/apidoc
        doins doc/doxy/html/*
    fi

    default
}

