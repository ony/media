# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=strukturag release=v${PV} suffix=tar.gz ]

SUMMARY="An open source implementation of the h.265 video codec"
DESCRIPTION="
It is written from scratch and has a plain C API to enable a simple
integration into other software.
libde265 supports WPP and tile-based multithreading and includes SSE
optimizations. The decoder includes all features of the Main profile and
correctly decodes almost all conformance streams."

HOMEPAGE+=" https://www.libde265.org/"

LICENCES="
    GPL-3  [[ note = [ sample applications ] ]]
    LGPL-3 [[ note = library ]]
"

SLOT="0"
MYOPTIONS="
    platform: amd64
    amd64_cpu_features: sse4.1
"

DEPENDENCIES=""

BUGS_TO="heirecka@exherbo.org"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # Disable example programs
    # Would need SDL
    --disable-dec265
    # Would neeed Qt and libvideogfx/ffmpeg (libswscale)
    --disable-sherlock265
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'amd64_cpu_features:sse4.1 sse'
)

