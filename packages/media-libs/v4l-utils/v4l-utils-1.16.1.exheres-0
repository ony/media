# Copyright 2009-2010 Ingmar Vanhassel
# Copyright 2011 Daniel Mierswa <impulze@impulze.org>
# Copyright 2012-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop gtk-icon-cache udev-rules \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Collection of libraries that add a thin abstraction layer on top of video4linux2 (V4L2) devices"
DESCRIPTION="
libv4l is a collection of libraries that adds a thin abstraction layer on top
of video4linux2 (V4L2) devices. The purpose of this layer is to make it easy
for application writers to support a wide variety of devices without having to
write separate code for different devices in the same class. It consists of 3
different libraries. libv4lconvert offers functions to convert from any (known)
pixel format to V4l2_PIX_FMT_BGR24 or V4l2_PIX_FMT_YUV420. libv4l1 offers the
(deprecated) v4l1 API on top of v4l2 devices, independent of the drivers for
those devices supporting v4l1 compatibility (which many v4l2 drivers do not).
libv4l2 offers the v4l2 API on top of v4l2 devices, while adding support for
the application transparent libv4lconvert conversion where necessary.
"
HOMEPAGE="https://www.linuxtv.org"
DOWNLOADS="${HOMEPAGE}/downloads/${PN}/${PNV}.tar.bz2"

LICENCES="GPL-2 || ( LGPL-2.1 LGPL-3 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    qt5 [[ description = [ Qt control panel application ] ]]

    ( libc: musl )
    ( providers: elfutils libelf ) [[ number-selected = exactly-one ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas: de pt_BR )
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        qt5? (
            sys-sound/alsa-lib
            x11-libs/qtbase:5[>=5.4][gui]
        )
        libc:musl? ( dev-libs/argp-standalone )
        providers:elfutils? ( dev-util/elfutils )
        providers:eudev? ( sys-apps/eudev )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libelf? ( dev-libs/libelf )
        providers:systemd? ( sys-apps/systemd )
    test:
        media-libs/SDL_image:2
        x11-dri/glu
        x11-dri/mesa[>=9.0]
        x11-libs/libX11
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-dyn-libv4l
    --enable-libdvbv5
    --enable-nls
    --enable-v4l-utils
    --enable-v4l2-compliance-libv4l
    --enable-v4l2-ctl-libv4l
    --enable-v4l2-ctl-stream-to
    --disable-gconv
    --disable-static
    --with-gconvdir=/usr/$(exhost --target)/lib/gconv
    --with-jpeg
    --with-udevdir="${UDEVDIR}"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'qt5 qv4l2' )

src_prepare() {
    # only works with Clang, last checked: 1.16.1
    edo sed \
        -e '/bpf_protocols/d' \
        -i utils/keytable/Makefile.am

    # TODO: report upstream
    if [[ $(exhost --target) == *-musl* ]];then
        expatch "${FILES}"/0001-fixup-lfs-mismatch-in-preload-libraries.patch
        expatch "${FILES}"/0003-musl.patch
    fi

    autotools_src_prepare
}

src_configure() {
    if [[ $(exhost --target) == *-musl* ]];then
        CFLAGS="${CPPFLAGS} -D__off_t=off_t -D__off64_t=off_t"
        LDFLAGS="${LDFLAGS} -largp"
    fi

    # Qt >= 5.7 requires at least C++11
    option qt5 && export CXXFLAGS="${CXXFLAGS} -std=c++11"

    default
}

src_install() {
    default

    edo rmdir "${IMAGE}"/etc/rc_keymaps
}

pkg_postinst() {
    option qt5 && freedesktop-desktop_pkg_postinst
    option qt5 && gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    option qt5 && freedesktop-desktop_pkg_postrm
    option qt5 && gtk-icon-cache_pkg_postrm
}

