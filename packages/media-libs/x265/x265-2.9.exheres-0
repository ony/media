# Copyright 2014-2016 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PNV/-/_}

require cmake [ api=2 ]

SUMMARY="Command line application and library for encoding video streams into the H.265/High Efficiency Video Coding (HEVC) format"
HOMEPAGE="http://${PN}.org"
DOWNLOADS="https://bitbucket.org/multicoreware/${PN}/downloads/${MY_PNV}.tar.gz"

BUGS_TO="mixi@exherbo.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

# linking TestBench fails with: undefined reference to x265_{alloc,free}_analysis_data
# last checked: 2.9
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/nasm[>=2.13.0]
"

CMAKE_SOURCE=${WORKBASE}/${MY_PNV}/source

# NOTE: Apparently 32-bit x86 together with HIGH_BIT_DEPTH are not supported by upstream.
# https://bitbucket.org/multicoreware/x265/issues/198/assembly-code-fails-on-x32
if [[ $(exhost --target) == i686-* ]]; then
    HIGH_BIT_DEPTH_ASM=0
else
    HIGH_BIT_DEPTH_ASM=1
fi

src_prepare() {
    cmake_src_prepare

    edo mkdir "${WORKBASE}"/build-{10,12}
}

src_configure() {
    local params=(
        -DENABLE_HDR10_PLUS:BOOL=FALSE
        -DENABLE_LIBNUMA:BOOL=FALSE
        -DENABLE_LIBVMAF:BOOL=FALSE
        $(expecting_tests -DENABLE_TESTS:BOOL=TRUE)
    )

    # http://x265.readthedocs.org/en/default/api.html#multi-library-interface -DEXPORT_C_API is
    # turned off so the libraries can be loaded into the same process without symbol name
    # conflicts

    edo pushd "${WORKBASE}"/build-12
    ecmake \
        "${params[@]}" \
        -DEXPORT_C_API:BOOL=FALSE \
        -DENABLE_SHARED:BOOL=FALSE \
        -DENABLE_CLI:BOOL=FALSE \
        -DHIGH_BIT_DEPTH:BOOL=TRUE \
        -DMAIN12:BOOL=TRUE \
        $([[ ${HIGH_BIT_DEPTH_ASM} == 1 ]] || echo "-DENABLE_ASSEMBLY:BOOL=FALSE")
    edo popd

    edo pushd "${WORKBASE}"/build-10
    ecmake \
        "${params[@]}" \
        -DEXPORT_C_API:BOOL=FALSE \
        -DENABLE_SHARED:BOOL=FALSE \
        -DENABLE_CLI:BOOL=FALSE \
        -DHIGH_BIT_DEPTH:BOOL=TRUE \
        $([[ ${HIGH_BIT_DEPTH_ASM} == 1 ]] || echo "-DENABLE_ASSEMBLY:BOOL=FALSE")
    edo popd

    # Create empty 12bit and 10bit libraries so configure doesn't fail. We'll properly build them in
    # the next phase.
    edo ${AR} rc libx265_main10.a
    edo ${AR} rc libx265_main12.a

    # Now build the main library with 8-bit as default target
    params+=(
        -DLINKED_10BIT:BOOL=TRUE
        -DLINKED_12BIT:BOOL=TRUE
        -DEXTRA_LIB:STRING="x265_main10.a;x265_main12.a"
        -DEXTRA_LINK_FLAGS:STRING=-L.
    )

    ecmake "${params[@]}"
}

src_compile() {
    edo pushd "${WORKBASE}"/build-12
    emake
    edo cp libx265.a "${WORKBASE}"/build/libx265_main12.a
    edo popd

    edo pushd "${WORKBASE}"/build-10
    emake
    edo cp libx265.a "${WORKBASE}"/build/libx265_main10.a
    edo popd

    default

    # Combine the libraries as in upstream's multilib.sh
    edo mv libx265{,_main}.a
    edo ${AR} -M <<EOF
CREATE libx265.a
ADDLIB libx265_main.a
ADDLIB libx265_main10.a
ADDLIB libx265_main12.a
SAVE
END
EOF
}

src_test() {
    edo pushd "${WORKBASE}"/build-12
    edo ./test/TestBench
    edo popd

    edo pushd "${WORKBASE}"/build-10
    edo ./test/TestBench
    edo popd

    edo ./test/TestBench
}

